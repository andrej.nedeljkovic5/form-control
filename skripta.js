function resetujGreske() {
    document.getElementById("userNameError").innerHTML = "";
    document.getElementById("emailerror").innerHTML = "";
    document.getElementById("Phoneerror").innerHTML = "";
    document.getElementById("passworderror").innerHTML = "";
    document.getElementById("confirmPassworderror").innerHTML = "";
}
 
function proveraPolja() {
    let userName = document.getElementById("userName").value;
    let email = document.getElementById("email").value;
    let Phone = document.getElementById("Phone").value;
    let password = document.getElementById("password").value;
    let confirmPassword = document.getElementById("confirmPassword").value;
 
    let userNameProvera = /^[a-zA-Z]\w+$/;
    let emailProvera = /^\w+@\w+\.[a-z]{2,3}$/; // abcde@student.rs
    if(userNameProvera.test(userName) == false) {
        document.getElementById("userNameError").innerHTML = "User name nije u dobrom formatu...";
        return false;
    }
    else if(emailProvera.test(email) == false) {
        document.getElementById("emailerror").innerHTML = "email nije u dobrom formatu...";
        return false;
    }
    // ...
    else if(password != confirmPassword) {
        document.getElementById("confirmPassworderror").innerHTML = "Lozinke se ne podudaraju...";
        return false;
    }
    // ...
    return true;
}
 
let nizKorisnika = []; // adrian, marko
 
function inicijalizujPodatke() {
    if (localStorage.getItem("korisnici") == null) {
        localStorage.setItem("korisnici", "");
    }
    else {
        let tekstNiza = localStorage.getItem("korisnici");
        let niz = JSON.parse(tekstNiza);
        nizKorisnika = niz;
    }
}
 
function proveriJedinstvenost() {
    let userName = document.getElementById("userName").value; // jelica
    for(korisnik of nizKorisnika) {
        if (userName == korisnik.userName) {
            document.getElementById("userNameError").innerHTML = "User name je već zauzeto.";
            return false;
        }
    }
    // ...
    return true;
}
 
function dodajKorisnika() {
    let userName = document.getElementById("userName").value;
    let email = document.getElementById("email").value;
    let Phone = document.getElementById("Phone").value;
    let password = document.getElementById("password").value;
 
    let korisnik = {
        "userName" : userName,
        "email" : email,
        "Phone" : Phone,
        "password" : password
    };
 
    nizKorisnika.push(korisnik);
 
    // JSON
    // tekst = stringify(element)
    // element = parse(tekst)
 
    let broj = 12345;
    let tekst = JSON.stringify(broj); // 12345 -> "12345"
    broj = JSON.parse(tekst);         // "12345" -> 12345
 
    let tekstNizaKorisnika = JSON.stringify(nizKorisnika); 
    localStorage.setItem("korisnici", tekstNizaKorisnika);
}
 
function registrujSe() {
    resetujGreske();
    let proveraOK = proveraPolja();
    if (proveraOK == true){
        let jedinstvenostOK = proveriJedinstvenost();
        if (jedinstvenostOK == true) {
            dodajKorisnika();
            alert("Korisnik uspesno dodat!");
        }
    }
}
 
function prijaviSe() {
    let userName = document.getElementById("userName").value;
    let password = document.getElementById("password").value;
 
    document.getElementById("userNameError").innerHTML = "";
    document.getElementById("passworderror").innerHTML = "";
 
    // adrian, marko, uros
    for(korisnik of nizKorisnika) {
        if (userName == korisnik.userName) {
            if(password == korisnik.password) {
                alert("Uspesno ste prijavljeni!");
                return;
            }
            else{
                document.getElementById("passworderror").innerHTML = "Uneta password je neispravna";
                return;
            }
        }
    }
    document.getElementById("userNameError").innerHTML = "Korisnicko ime ne postoji";
}